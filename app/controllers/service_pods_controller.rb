class ServicePodsController < ApplicationController
  before_action :set_service_pod, only: [:show, :update, :destroy]

  # GET /service_pods
  def index
    @service_pods = ServicePod.all

    render json: @service_pods
  end

  # GET /service_pods/1
  def show
    render json: @service_pod
  end

  # POST /service_pods
  def create
    @service_pod = ServicePod.new(service_pod_params)

    if @service_pod.save
      render json: @service_pod, status: :created, location: @service_pod
    else
      render json: @service_pod.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /service_pods/1
  def update
    if @service_pod.update(service_pod_params)
      render json: @service_pod
    else
      render json: @service_pod.errors, status: :unprocessable_entity
    end
  end

  # DELETE /service_pods/1
  def destroy
    @service_pod.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_pod
      @service_pod = ServicePod.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def service_pod_params
      params.require(:service_pod).permit(:brother_id, :semester_id, :name)
    end
end
