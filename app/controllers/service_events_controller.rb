class ServiceEventsController < ApplicationController
  before_action :set_service_event, only: [:show, :update, :destroy]

  # GET /service_events
  def index
    @service_events = ServiceEvent.all

    render json: @service_events
  end

  # GET /service_events/1
  def show
    render json: @service_event
  end

  # POST /service_events
  def create
    @service_event = ServiceEvent.new(service_event_params)

    if @service_event.save
      render json: @service_event, status: :created, location: @service_event
    else
      render json: @service_event.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /service_events/1
  def update
    if @service_event.update(service_event_params)
      render json: @service_event
    else
      render json: @service_event.errors, status: :unprocessable_entity
    end
  end

  # DELETE /service_events/1
  def destroy
    @service_event.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_event
      @service_event = ServiceEvent.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def service_event_params
      params.require(:service_event).permit(:service_pod_id, :semester_id, :description, :duration)
    end
end
