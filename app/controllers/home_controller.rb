class HomeController < ApplicationController
  def index
    render json: { message: 'You are not authenticated' }
  end
end
