class RushEventsController < ApplicationController
  before_action :set_rush_event, only: [:show, :update, :destroy]

  # GET /rush_events
  def index
    @rush_events = RushEvent.all

    render json: @rush_events
  end

  # GET /rush_events/1
  def show
    render json: @rush_event
  end

  # POST /rush_events
  def create
    @rush_event = RushEvent.new(rush_event_params)

    if @rush_event.save
      render json: @rush_event, status: :created, location: @rush_event
    else
      render json: @rush_event.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /rush_events/1
  def update
    if @rush_event.update(rush_event_params)
      render json: @rush_event
    else
      render json: @rush_event.errors, status: :unprocessable_entity
    end
  end

  # DELETE /rush_events/1
  def destroy
    @rush_event.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rush_event
      @rush_event = RushEvent.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def rush_event_params
      params.require(:rush_event).permit(:semester_id, :title, :description, :date)
    end
end
