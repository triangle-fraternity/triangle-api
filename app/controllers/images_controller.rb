class ImagesController < ApplicationController
  before_action :set_image, only: [:show, :update, :destroy]

  def options
    puts 'Yo wat up fam'
  end

  # GET /images
  def index
    @images = Image.all.map do |image|
      { id: image.id, url: image.picture.url }
    end

    render json: @images
  end

  # GET /images/1
  def show
    render json: { id: @image.id, url: @image.picture.url }
  end

  # POST /images
  def create
    p image_params
    @image = Image.new(image_params)

    if @image.save
      render json: @image, status: :created, location: @image
    else
      render json: @image.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /images/1
  def update
    if @image.update(image_params)
      render json: @image
    else
      render json: @image.errors, status: :unprocessable_entity
    end
  end

  # DELETE /images/1
  def destroy
    @image.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_image
    @image = Image.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def image_params
    { picture: params.permit(:file)[:file] }
  end
end
