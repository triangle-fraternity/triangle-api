class SessionsController < Devise::SessionsController
  respond_to :html, :json

  def create
    super do |user|
      if request.format.json?
        data = {
          token: user.auth_token,
          email: user.email
        }
        render(json: data, status: 201) && return
      end
    end
  end
end
