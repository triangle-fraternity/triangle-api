class Brother < ApplicationRecord
  has_and_belongs_to_many :semesters
  has_one :job
  has_one :service_pod
end
