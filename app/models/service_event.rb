class ServiceEvent < ApplicationRecord
  belongs_to :service_pod
  belongs_to :semester
end
