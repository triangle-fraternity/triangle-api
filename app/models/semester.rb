class Semester < ApplicationRecord
  has_and_belongs_to_many :brothers
  has_many :service_pods
  has_many :service_events
  has_many :rush_events
end
