class Image < ApplicationRecord
  has_attached_file :picture, default_url: '/images/george.jpg'
  validates_attachment :picture,
                       content_type: { content_type: ['image/jpeg', 'image/png'] },
                       size: { less_than: 5.megabytes },
                       file_name: { matches: [/png\Z/, /jpe?g\Z/] }
  do_not_validate_attachment_file_type :picture
end
