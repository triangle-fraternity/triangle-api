class ServicePod < ApplicationRecord
  belongs_to :brother
  belongs_to :semester
  has_many :service_events
end
