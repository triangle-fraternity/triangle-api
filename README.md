# Triangle-api

This is the backend for the website. It will handle all of the security and 
data for our web apps. This backend is serving up json to triangle-web, which
will handle the UX

## Prerequisites

You will need the following things properly installed on your computer.
* Git
* Ruby 2.3 (pls use RVM)
* Rails 5 (pls use RVM)
* RVM?!
* Postgresql

## Installation

* git clone this repo into the same directory you will be cloning triangle-web
* `cd triangle-api`
* `bundle install`

## To setup the database
* `rails db:drop db:create db:migrate db:seed`

## To serve on localhost:3000
* `rails s`

### Running Tests

* `rails test`

### Deploying

* `git push heroku master`
* You probably also want to do something on the prod database
  * Use `heroku run rails [db:migrate or something]`
  * Another useful/dangerous one `heroku pg:reset DATABASE --confirm vast-journey-97799`
