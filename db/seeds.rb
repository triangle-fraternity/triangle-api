# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
puts 'Seeding the database'
user = User.create!(email: 'seed@email.com', password: 12_345_678)

images = [
  { picture: File.new('public/images/bbq.jpg', 'r') },
  { picture: File.new('public/images/casino.jpeg', 'r') },
  { picture: File.new('public/images/circuit.jpg', 'r') },
  { picture: File.new('public/images/dodgeball.jpeg', 'r') },
  { picture: File.new('public/images/dodgeball2.jpeg', 'r') },
  { picture: File.new('public/images/friday_football.jpeg', 'r') },
  { picture: File.new('public/images/fruit_ninja.jpeg', 'r') },
  { picture: File.new('public/images/luau.jpg', 'r') },
  { picture: File.new('public/images/tailgating.jpeg', 'r') },
  { picture: File.new('public/images/wings.jpg', 'r') }

]
Image.create!(images)

brothers = [
  { first_name: 'George ', middle_name: 'Mani', last_name: 'Varghese', major: 'Mechanical Engineering', pledge_class: 'Squirrelmen', initials: 'GMV', exec: 'Vice President', positions: '' },
  { first_name: 'Lee', middle_name: '"Eray"', last_name: 'Gill', major: 'Electrical Engineering', pledge_class: 'Squirrelmen', initials: 'LEG', exec: '', positions: '' },
  { first_name: 'Luvsandondov', middle_name: '"LJ"', last_name: 'Lkhamsuren', major: 'Computer Science', pledge_class: 'Squirrelmen', initials: 'LLL', exec: '', positions: '' },
  { first_name: 'Marcus ', middle_name: 'John', last_name: 'Voegele', major: 'Mechanical Engineering', pledge_class: 'Squirrelmen', initials: 'MJV', exec: '', positions: '' },
  { first_name: 'Nathan ', middle_name: 'Henry', last_name: 'Davis', major: 'Computer Engineering', pledge_class: 'Squirrelmen', initials: 'NHD', exec: '', positions: '' },
  { first_name: 'Adam', middle_name: 'Brian', last_name: 'Barbato', major: 'Computer Science', pledge_class: 'Polarmen', initials: 'ABB', exec: '', positions: '' },
  { first_name: 'Ali', middle_name: '"P^$$% Annhilator"', last_name: 'El Ashri', major: 'Aerospace Engineering', pledge_class: 'Polarmen', initials: 'APE', exec: '', positions: '' },
  { first_name: 'Christian', middle_name: 'James', last_name: 'Vanhooser', major: 'Civil Engineering', pledge_class: 'Polarmen', initials: 'CJV', exec: '', positions: 'Brotherhood, Recruitment' },
  { first_name: 'Dawith', middle_name: '"Umbreon"', last_name: 'Ha', major: 'Computer Engineering', pledge_class: 'Polarmen', initials: 'DUH', exec: '', positions: '' },
  { first_name: 'Dylan', middle_name: 'Michael', last_name: 'Futrell', major: 'Mechanical Engineering', pledge_class: 'Polarmen', initials: 'DMF', exec: '', positions: '' },
  { first_name: 'Hansel', middle_name: 'Chalid', last_name: 'Sjukur', major: 'Mechanical Engineering', pledge_class: 'Polarmen', initials: 'HCS', exec: '', positions: '' },
  { first_name: 'Hubert', middle_name: 'Theodore', last_name: 'Jani', major: 'Computer Science', pledge_class: 'Polarmen', initials: 'HTJ', exec: '', positions: '' },
  { first_name: 'Jonathan', middle_name: 'Michael', last_name: 'Rakushin-Weinstein', major: 'Electrical Engineering', pledge_class: 'Polarmen', initials: 'JMR', exec: '', positions: 'Membership Development' },
  { first_name: 'Kevin', middle_name: 'James', last_name: 'Scheer', major: 'Computer Engineering', pledge_class: 'Polarmen', initials: 'KJS', exec: '', positions: '' },
  { first_name: 'Mathew', middle_name: 'Scott', last_name: 'Halm', major: 'Computer Science', pledge_class: 'Polarmen', initials: 'MSH', exec: '', positions: '' },
  { first_name: 'Michael', middle_name: 'Charles', last_name: 'Spinuzza', major: 'Mechanical Engineering', pledge_class: 'Polarmen', initials: 'MCS', exec: '', positions: '' },
  { first_name: 'Minwoo', middle_name: 'Jack', last_name: 'Eom', major: 'Civil Engineering', pledge_class: 'Polarmen', initials: 'MJE', exec: '', positions: '' },
  { first_name: 'Trevor', middle_name: 'Ryan', last_name: 'Vogler', major: 'Civil Engineering', pledge_class: 'Polarmen', initials: 'TRV', exec: '', positions: '' },
  { first_name: 'William', middle_name: 'David', last_name: 'Mercado', major: 'Electrical Engineering', pledge_class: 'Polarmen', initials: 'WDM', exec: '', positions: 'Risk Manager' },
  { first_name: 'Alex', middle_name: 'Vincent', last_name: 'Pronger', major: 'Aerospace Engineering', pledge_class: 'Frenchmen', initials: 'AVP', exec: '', positions: '' },
  { first_name: 'Henri', middle_name: 'Lucas', last_name: 'Montisci', major: 'Civil Engineering', pledge_class: 'Frenchmen', initials: 'HLM', exec: 'Secretary', positions: '' },
  { first_name: 'Jason', middle_name: 'Brett', last_name: 'Wilensky', major: 'Electrical Engineering', pledge_class: 'Frenchmen', initials: 'JBW', exec: 'Treasurer', positions: '' },
  { first_name: 'Kacper', middle_name: 'Jozef', last_name: 'Lachowski', major: 'Materials Science', pledge_class: 'Frenchmen', initials: 'KJL', exec: '', positions: 'Service Chair' },
  { first_name: 'Alec ', middle_name: 'Richard', last_name: 'Biesterfeld', major: '', pledge_class: 'Forestmen', initials: 'ARB', exec: '', positions: 'Academics Chair' },
  { first_name: 'Ali', middle_name: 'Arsalan', last_name: 'Yaqoob', major: '', pledge_class: 'Forestmen', initials: 'AAY', exec: '', positions: '' },
  { first_name: 'Daniel', middle_name: 'Tristan', last_name: 'Margosian', major: 'Computer Science', pledge_class: 'Forestmen', initials: 'DTM', exec: '', positions: '' },
  { first_name: 'David', middle_name: 'Yichen', last_name: 'Liu', major: '', pledge_class: 'Forestmen', initials: 'DYL', exec: '', positions: '' },
  { first_name: 'Fares', middle_name: '"Please let me"', last_name: 'Takieddine', major: '', pledge_class: 'Forestmen', initials: 'FPT', exec: '', positions: '' },
  { first_name: 'Jay', middle_name: 'Eun jae', last_name: 'Kim', major: '', pledge_class: 'Forestmen', initials: 'JEK', exec: '', positions: '' },
  { first_name: 'Jesse ', middle_name: 'William', last_name: 'Seibert', major: 'Aerospace Engineering', pledge_class: 'Forestmen', initials: 'JWS', exec: '', positions: '' },
  { first_name: 'Michael', middle_name: 'Alan', last_name: 'Osadjan', major: '', pledge_class: 'Forestmen', initials: 'MAO', exec: '', positions: 'Asst. Houseman' },
  { job_id: 6, first_name: 'Michael', middle_name: 'Richard', last_name: 'Rupp', major: 'Electrical Engineering', pledge_class: 'Forestmen', initials: 'MRR', exec: '', positions: 'Recruitment Chair' },
  { job_id: 5, first_name: 'Patrick', middle_name: 'Joseph', last_name: 'Burke', major: 'Mechanical Engineering', pledge_class: 'Forestmen', initials: 'PJB', exec: '', positions: 'Social' },
  { first_name: 'Sean', middle_name: 'Colin', last_name: 'Mercer-Smith', major: '', pledge_class: 'Forestmen', initials: 'SCM', exec: '', positions: '' },
  { first_name: 'Stephen', middle_name: 'Joseph', last_name: 'Westman', major: '', pledge_class: 'Forestmen', initials: 'SJW', exec: '', positions: '' },
  { first_name: 'Thomas', middle_name: 'James', last_name: 'Diamantoponlos', major: 'Engineering Physics', pledge_class: 'Forestmen', initials: 'AJD', exec: '', positions: 'Social' },
  { first_name: 'Tyler', middle_name: 'Zhi', last_name: 'Loo', major: '', pledge_class: 'Forestmen', initials: 'TZL', exec: '', positions: '' },
  { job_id: 8, first_name: 'Brian', middle_name: 'James', last_name: 'Andersen', major: 'Electrical Engineering', pledge_class: 'Desertmen', initials: 'BJA', exec: '', positions: '' },
  { first_name: 'Dan', middle_name: 'Tomasz', last_name: 'Jamrozik', major: 'Computer Science', pledge_class: 'Desertmen', initials: 'DTJ', exec: '', positions: '' },
  { first_name: 'David', middle_name: 'Alexander', last_name: 'Lisk', major: 'Mechanical Engineering', pledge_class: 'Desertmen', initials: 'DAL', exec: '', positions: 'Historian' },
  { first_name: 'Frank ', middle_name: '"Motherfucking"', last_name: 'Long', major: 'General Engineering', pledge_class: 'Desertmen', initials: 'FML', exec: '', positions: '' },
  { job_id: 3, first_name: 'Jacob', middle_name: 'Walker', last_name: 'Trueb', major: 'Computer Science', pledge_class: 'Desertmen', initials: 'JWT', exec: '', positions: 'PR Chair, Recruitment Chair' },
  { job_id: 1, first_name: 'Matthew', middle_name: 'Annam', last_name: 'Ho', major: 'Engineering Physics', pledge_class: 'Desertmen', initials: 'MAH', exec: 'Social', positions: '' },
  { first_name: 'Mrunal', middle_name: 'Jatin', last_name: 'Sarvaiya', major: '', pledge_class: 'Desertmen', initials: 'MJS', exec: '', positions: '' },
  { first_name: 'Ryan', middle_name: 'Christopher', last_name: 'Kuck', major: 'Computer Science', pledge_class: 'Desertmen', initials: 'RCK', exec: '', positions: '' },
  { first_name: 'Vibhu', middle_name: 'T', last_name: 'Vanjari', major: 'Electrical Engineering', pledge_class: 'Desertmen', initials: 'VTV', exec: '', positions: 'Recruitment Chair, EC Rep' },
  { first_name: 'Gagandeep ', middle_name: "PleaseDon't", last_name: 'Singh', major: 'Technical Systems Management', pledge_class: 'Brashmen', initials: 'GPS', exec: '', positions: '' },
  { first_name: 'Heyuan', middle_name: 'HiYah', last_name: 'Huang', major: 'Mechanical Engineering', pledge_class: 'Brashmen', initials: 'HHH', exec: '', positions: '' },
  { first_name: 'Kenneth ', middle_name: 'Ivär', last_name: 'Larson', major: 'Technical Systems Management', pledge_class: 'Brashmen', initials: 'KIL', exec: '', positions: 'Commisar' },
  { first_name: 'Mitchell', middle_name: 'Glenn', last_name: 'Nahikian', major: 'Physics', pledge_class: 'Brashmen', initials: 'MGN', exec: 'Houseman', positions: 'Pledge Ed' },
  { first_name: 'Sang Hoon ', middle_name: 'Eric', last_name: 'Hong', major: 'Mechanical Engineering', pledge_class: 'Brashmen', initials: 'SEH', exec: '', positions: '' },
  { job_id: 7, first_name: 'Andrew', middle_name: 'Thomas', last_name: 'Cassiere', major: 'Mechanical Engineering', pledge_class: 'Anchormen', initials: 'ATC', exec: '', positions: 'Asst. Houseman' },
  { first_name: 'Austin', middle_name: 'Patrick', last_name: 'Scott', major: 'Aerospace Engineering', pledge_class: 'Anchormen', initials: 'APS', exec: '', positions: '' },
  { first_name: 'Bora', middle_name: 'a', last_name: 'Basa', major: 'Electrical Engineering', pledge_class: 'Anchormen', initials: 'BBB', exec: '', positions: '' },
  { first_name: 'Brian', middle_name: 'Thomas', last_name: 'Hinterlong', major: 'Physics', pledge_class: 'Anchormen', initials: 'BTH', exec: '', positions: '' },
  { first_name: 'Fred', middle_name: 'Robert', last_name: 'Williams', major: 'Computer Engineering', pledge_class: 'Anchormen', initials: 'FRW', exec: '', positions: '' },
  { first_name: 'Joseph', middle_name: 'Alexander', last_name: 'Mitchell', major: 'Nuclear Engineering', pledge_class: 'Anchormen', initials: 'JAM', exec: '', positions: '' },
  { first_name: 'Josh', middle_name: 'David', last_name: 'Ryan', major: 'Electrical Engineering', pledge_class: 'Anchormen', initials: 'JDR', exec: 'External', positions: '' },
  { first_name: 'Kassidy', middle_name: 'Yuanxue', last_name: 'Zhou', major: 'Bioengineering', pledge_class: 'Anchormen', initials: 'KYZ', exec: '', positions: 'Philanthropy Chair' },
  { job_id: 2, first_name: 'Kyle', middle_name: 'Chandler', last_name: 'Coffland', major: 'Engineering Physics', pledge_class: 'Anchormen', initials: 'KCC', exec: '', positions: 'Asst. Houseman' },
  { first_name: 'Riley', middle_name: 'Elis', last_name: 'Vesto', major: 'Physics', pledge_class: 'Anchormen', initials: 'REV', exec: '', positions: 'Tech Chair' },
  { job_id: 4, first_name: 'Trevor', middle_name: 'Quinlan', last_name: 'Brady', major: 'Chemical Engineering', pledge_class: 'Anchormen', initials: 'TQB', exec: 'Recruitment', positions: '' }
]
Brother.create!(brothers)

jobs = [{ brother_id: 43, title: 'President', execb: 1, email: 'triangle.uiuc.pres@gmail.com' },
        { brother_id: 60, title: 'Vice President', execb: 2, email: 'triangle.uiuc.ivp@gmail.com' },
        { brother_id: 42, title: 'External Director', execb: 3, email: 'triangle.uiuc.evp@gmail.com' },
        { brother_id: 62, title: 'Recruitment	Director', execb: 4, email: 'triangle.uiuc.rvp@gmail.com' },
        { brother_id: 33, title: 'Social Director', execb: 5, email: 'triangle.uiuc.social@gmail.com' },
        { brother_id: 32, title: 'Treasurer', execb: 6, email: 'triangle.uiuc.tres@gmail.com' },
        { brother_id: 52, title: 'House Manager', execb: 7, email: 'triangle.uiuc.houseman@gmail.com' },
        { brother_id: 38, title: 'Secretary', execb: 8, email: 'triangle.uiuc.sec@gmail.com' },
        { brother_id: 9, title: 'Brotherhood Chair', execb: 0 },
        { brother_id: 9, title: 'Recruitment Chair', execb: 0 },
        { brother_id: 9, title: 'Membership Development Chair', execb: 0 },
        { brother_id: 9, title: 'Risk Manager', execb: 0 },
        { brother_id: 9, title: 'Treasurer', execb: 0 },
        { brother_id: 9, title: 'Service Chair', execb: 0 },
        { brother_id: 9, title: 'Academics Chair', execb: 0 },
        { brother_id: 9, title: 'Assistant Houseman', execb: 0 },
        { brother_id: 9, title: 'Recruitment Chair', execb: 0 },
        { brother_id: 9, title: 'Social Chair', execb: 0 },
        { brother_id: 9, title: 'Historian', execb: 0 },
        { brother_id: 9, title: 'Public Relations Chair', execb: 0 },
        { brother_id: 9, title: 'Recruitment Chair', execb: 0 },
        { brother_id: 9, title: 'Engineering Council Representative', execb: 0 },
        { brother_id: 9, title: 'Commisar', execb: 0 },
        { brother_id: 9, title: 'Pledge Educator', execb: 0 },
        { brother_id: 9, title: 'Philanthropy Chair', execb: 0 },
        { brother_id: 9, title: 'Tech Chair', execb: 0 }]
Job.create!(jobs)

semesters = [{ term: 'Fall', year: 2012 },
             { term: 'Spring', year: 2012 },
             { term: 'Fall', year: 2012 },
             { term: 'Spring', year: 2013 },
             { term: 'Fall', year: 2013 },
             { term: 'Spring', year: 2014 },
             { term: 'Fall', year: 2014 },
             { term: 'Spring', year: 2015 },
             { term: 'Fall', year: 2015 },
             { term: 'Spring', year: 2016 },
             { term: 'Fall', year: 2016 }]
Semester.create!(semesters)

current_semester = Semester.last
rush_events = [{ semester_id: current_semester.id, image_id: 1, title: 'Post Quad Day BBQ', description: 'Come joins us as we celebrate the begninning of a new school year. Get a chance to meet all our brothers and make new connections', date: 'Sunday the 21st @ 5pm' },
               { semester_id: current_semester.id, image_id: 10, title: 'Wings and Rings', description: 'What more is there to say! We have a great wing night planned full of fun an games', date: 'Tuesday the 23rd @ 7pm' },
               { semester_id: current_semester.id, image_id: 7, title: 'Fruit Ninja', description: 'TBT to 2010 when Fruit Ninja was the hottest game to play', date: 'Thursday the 25th @ 6pm' },
               { semester_id: current_semester.id, image_id: 3, title: 'Triangle Circuit', description: "Who doesn't love to play sports. Test your ability in the Triangle Circuit. A set of different sports in one day with one goal ... WIN!", date: 'Saturday the 27th @ 12pm' },
               { semester_id: current_semester.id, image_id: 4, title: 'Dodgeball', description: "If your competitive drive was not sufficed by Saturday's Games come join the brothers for dodgeball", date: 'Tuesday the 30th @ 6pm' },
               { semester_id: current_semester.id, image_id: 2, title: 'Casino Royale', description: 'Bring your poker face, your hoodie, black glasses and most importantly the killer instinct to destroy anyone at the table as you compete to clean the table', date: 'Thursday the 1st @8pm' },
               { semester_id: current_semester.id, image_id: 9, title: 'UIUC Football Tailgating', description: 'I-L-L! I-N-I! Join us as we head out to the field to support and back our team to VICTORY!', date: 'Saturday the 3rd @ 12pm' },
               { semester_id: current_semester.id, image_id: 8, title: 'Labor Day Luau', description: 'Come for a chill party while we enjoy the first holiday of the year', date: 'Sunday the 4th @ 5pm' },
               { semester_id: current_semester.id, image_id: 5, title: 'Dodgeball', description: 'Come hold on to your bragging rights as we rollout the dodgeball event v2.0', date: 'Wednesday the 7th @ 6pm' },
               { semester_id: current_semester.id, image_id: 6, title: 'Friday Night Football', description: 'Join us as we cheer on the orange and blue to bring home the win under the Friday Night lights', date: 'Friday the 9th @ 6pm' }]
RushEvent.create!(rush_events)
