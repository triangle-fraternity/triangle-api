class AddImageIdToRushEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :rush_events, :image_id, :integer
  end
end
