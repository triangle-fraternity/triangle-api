class CreateRushEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :rush_events do |t|
      t.belongs_to :semester, foreign_key: true
      t.string :title
      t.string :description
      t.string :date

      t.timestamps
    end
  end
end
