class AddRushToJobs < ActiveRecord::Migration[5.0]
  def change
    add_column :jobs, :rush, :boolean, default: false
  end
end
