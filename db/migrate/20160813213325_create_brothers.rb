class CreateBrothers < ActiveRecord::Migration[5.0]
  def change
    create_table :brothers do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :major
      t.string :pledge_class
      t.string :initials
      t.integer :exec
      t.string :positions
      t.integer :job_id, optional: true
      t.timestamps
    end
  end
end
