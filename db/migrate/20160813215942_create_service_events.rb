class CreateServiceEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :service_events do |t|
      t.belongs_to :service_pod, foreign_key: true
      t.belongs_to :semester, foreign_key: true
      t.string :description
      t.float :duration

      t.timestamps
    end
  end
end
