class CreateServicePods < ActiveRecord::Migration[5.0]
  def change
    create_table :service_pods do |t|
      t.belongs_to :brother, foreign_key: true
      t.belongs_to :semester, foreign_key: true
      t.string :name

      t.timestamps
    end
  end
end
