class CreateJobs < ActiveRecord::Migration[5.0]
  def change
    create_table :jobs do |t|
      t.belongs_to :brother, foreign_key: true, optional: true
      t.string :title

      t.timestamps
    end
  end
end
