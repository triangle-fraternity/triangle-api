class AddExecbColumnToJobs < ActiveRecord::Migration[5.0]
  def change
    add_column :jobs, :execb, :boolean, default: false
  end
end
