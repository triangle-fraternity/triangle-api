class CreateJoinTable < ActiveRecord::Migration[5.0]
  def change
    create_join_table :brothers, :semester do |t|
      t.index [:brother_id, :semester_id]
      t.index [:semester_id, :brother_id]
    end
  end
end
