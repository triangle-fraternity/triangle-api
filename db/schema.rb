# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160817184733) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "brothers", force: :cascade do |t|
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "major"
    t.string   "pledge_class"
    t.string   "initials"
    t.integer  "exec"
    t.string   "positions"
    t.integer  "job_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "brothers_semester", id: false, force: :cascade do |t|
    t.integer "brother_id",  null: false
    t.integer "semester_id", null: false
    t.index ["brother_id", "semester_id"], name: "index_brothers_semester_on_brother_id_and_semester_id", using: :btree
    t.index ["semester_id", "brother_id"], name: "index_brothers_semester_on_semester_id_and_brother_id", using: :btree
  end

  create_table "images", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "rush_event_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.index ["rush_event_id"], name: "index_images_on_rush_event_id", using: :btree
    t.index ["user_id"], name: "index_images_on_user_id", using: :btree
  end

  create_table "jobs", force: :cascade do |t|
    t.integer  "brother_id"
    t.string   "title"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "execb",      default: false
    t.string   "email"
    t.boolean  "rush",       default: false
    t.index ["brother_id"], name: "index_jobs_on_brother_id", using: :btree
  end

  create_table "rush_events", force: :cascade do |t|
    t.integer  "semester_id"
    t.string   "title"
    t.string   "description"
    t.string   "date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "image_id"
    t.index ["semester_id"], name: "index_rush_events_on_semester_id", using: :btree
  end

  create_table "semesters", force: :cascade do |t|
    t.integer  "year"
    t.string   "term"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_events", force: :cascade do |t|
    t.integer  "service_pod_id"
    t.integer  "semester_id"
    t.string   "description"
    t.float    "duration"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["semester_id"], name: "index_service_events_on_semester_id", using: :btree
    t.index ["service_pod_id"], name: "index_service_events_on_service_pod_id", using: :btree
  end

  create_table "service_pods", force: :cascade do |t|
    t.integer  "brother_id"
    t.integer  "semester_id"
    t.string   "name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["brother_id"], name: "index_service_pods_on_brother_id", using: :btree
    t.index ["semester_id"], name: "index_service_pods_on_semester_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "auth_token"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "images", "rush_events"
  add_foreign_key "images", "users"
  add_foreign_key "jobs", "brothers"
  add_foreign_key "rush_events", "semesters"
  add_foreign_key "service_events", "semesters"
  add_foreign_key "service_events", "service_pods"
  add_foreign_key "service_pods", "brothers"
  add_foreign_key "service_pods", "semesters"
end
