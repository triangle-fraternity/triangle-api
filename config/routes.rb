Rails.application.routes.draw do
  root to: 'home#index'
  resources :images
  match '/images' => 'images#options', via: :options
  resources :rush_events
  resources :service_events
  resources :service_pods
  resources :jobs
  resources :semesters
  resources :brothers
  devise_for :users, controllers: { sessions: 'sessions' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
