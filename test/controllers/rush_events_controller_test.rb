require 'test_helper'

class RushEventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rush_event = rush_events(:one)
  end

  test "should get index" do
    get rush_events_url, as: :json
    assert_response :success
  end

  test "should create rush_event" do
    assert_difference('RushEvent.count') do
      post rush_events_url, params: { rush_event: { date: @rush_event.date, description: @rush_event.description, semester_id: @rush_event.semester_id, title: @rush_event.title } }, as: :json
    end

    assert_response 201
  end

  test "should show rush_event" do
    get rush_event_url(@rush_event), as: :json
    assert_response :success
  end

  test "should update rush_event" do
    patch rush_event_url(@rush_event), params: { rush_event: { date: @rush_event.date, description: @rush_event.description, semester_id: @rush_event.semester_id, title: @rush_event.title } }, as: :json
    assert_response 200
  end

  test "should destroy rush_event" do
    assert_difference('RushEvent.count', -1) do
      delete rush_event_url(@rush_event), as: :json
    end

    assert_response 204
  end
end
