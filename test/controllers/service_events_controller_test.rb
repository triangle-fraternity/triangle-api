require 'test_helper'

class ServiceEventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @service_event = service_events(:one)
  end

  test "should get index" do
    get service_events_url, as: :json
    assert_response :success
  end

  test "should create service_event" do
    assert_difference('ServiceEvent.count') do
      post service_events_url, params: { service_event: { description: @service_event.description, duration: @service_event.duration, semester_id: @service_event.semester_id, service_pod_id: @service_event.service_pod_id } }, as: :json
    end

    assert_response 201
  end

  test "should show service_event" do
    get service_event_url(@service_event), as: :json
    assert_response :success
  end

  test "should update service_event" do
    patch service_event_url(@service_event), params: { service_event: { description: @service_event.description, duration: @service_event.duration, semester_id: @service_event.semester_id, service_pod_id: @service_event.service_pod_id } }, as: :json
    assert_response 200
  end

  test "should destroy service_event" do
    assert_difference('ServiceEvent.count', -1) do
      delete service_event_url(@service_event), as: :json
    end

    assert_response 204
  end
end
