require 'test_helper'

class BrothersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @brother = brothers(:one)
  end

  test "should get index" do
    get brothers_url, as: :json
    assert_response :success
  end

  test "should create brother" do
    assert_difference('Brother.count') do
      post brothers_url, params: { brother: { major: @brother.major, name: @brother.name, pledge_class: @brother.pledge_class, user_id: @brother.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show brother" do
    get brother_url(@brother), as: :json
    assert_response :success
  end

  test "should update brother" do
    patch brother_url(@brother), params: { brother: { major: @brother.major, name: @brother.name, pledge_class: @brother.pledge_class, user_id: @brother.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy brother" do
    assert_difference('Brother.count', -1) do
      delete brother_url(@brother), as: :json
    end

    assert_response 204
  end
end
