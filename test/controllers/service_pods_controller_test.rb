require 'test_helper'

class ServicePodsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @service_pod = service_pods(:one)
  end

  test "should get index" do
    get service_pods_url, as: :json
    assert_response :success
  end

  test "should create service_pod" do
    assert_difference('ServicePod.count') do
      post service_pods_url, params: { service_pod: { brother_id: @service_pod.brother_id, name: @service_pod.name, semester_id: @service_pod.semester_id } }, as: :json
    end

    assert_response 201
  end

  test "should show service_pod" do
    get service_pod_url(@service_pod), as: :json
    assert_response :success
  end

  test "should update service_pod" do
    patch service_pod_url(@service_pod), params: { service_pod: { brother_id: @service_pod.brother_id, name: @service_pod.name, semester_id: @service_pod.semester_id } }, as: :json
    assert_response 200
  end

  test "should destroy service_pod" do
    assert_difference('ServicePod.count', -1) do
      delete service_pod_url(@service_pod), as: :json
    end

    assert_response 204
  end
end
